package org.usfirst.frc.team1369.robot;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;

/**
 * Created by admin on 3/27/16.
 */

public class ADXGyro extends ADXRS450_Gyro
{

    private double relativeAngle;

    public ADXGyro()
    {
        super();
        reset();
        this.relativeAngle = 0.0;
    }

    public double getRelativeAngle()
    {
        return relativeAngle;
    }

}
