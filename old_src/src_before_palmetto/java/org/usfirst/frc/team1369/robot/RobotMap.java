package org.usfirst.frc.team1369.robot;

import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.interfaces.Potentiometer;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap 
{

	/* Timer **/
    public static boolean timerDone = false;
    public static void setTimerDone(boolean kek)
    {
        timerDone = kek;
    }

    /* Motors */
    
    //Ports
    public static final int PORT_LEFT_MOTOR_FRONT = 0;
    public static final int PORT_LEFT_MOTOR_BACK = 2;
    public static final int PORT_RIGHT_MOTOR_FRONT = 1;
    public static final int PORT_RIGHT_MOTOR_BACK = 3;
    public static final int PORT_LEFT_MOTOR_SHOOTER = 4;
    public static final int PORT_RIGHT_MOTOR_SHOOTER = 5;
    public static final int PORT_MOTOR_LIFT = 6;
    public static final int PORT_MOTOR_HANG = 7;

    //public static Encoder MOTOR_ENCODER_HANG = new Encoder(2,3,false, Encoder.EncodingType.k4X);

    //Motor Controllers
    public static Talon MOTOR_LEFT_MOTOR_FRONT = new Talon(PORT_LEFT_MOTOR_FRONT);
    public static Talon MOTOR_LEFT_MOTOR_BACK = new Talon(PORT_LEFT_MOTOR_BACK);
    public static Talon MOTOR_RIGHT_MOTOR_FRONT = new Talon(PORT_RIGHT_MOTOR_FRONT);
    public static Talon MOTOR_RIGHT_MOTOR_BACK = new Talon(PORT_RIGHT_MOTOR_BACK);
    public static Talon MOTOR_LEFT_MOTOR_SHOOTER = new Talon(PORT_LEFT_MOTOR_SHOOTER);
    public static Talon MOTOR_RIGHT_MOTOR_SHOOTER = new Talon(PORT_RIGHT_MOTOR_SHOOTER);
    public static Talon MOTOR_LIFT = new Talon(PORT_MOTOR_LIFT);
    public static Talon MOTOR_HANG = new Talon(PORT_MOTOR_HANG);
    //public static Encoder MOTOR_ENCODER_LEFT = new Encoder(0,1,false,Encoder.EncodingType.k4X);

    //Motor Speeds
    public static double LIFT_SPEED = 0.5;
    public static double SHOOTER_SPEED = 0.2;
    public static double DRIVE_MOTOR_SPEED = 0.5;
    public static final double INTAKE_MOTOR_SLOW_SPEED = 0.5;

    public static DigitalInput LIMIT_LIFT = new DigitalInput(1);

    /* JOYSTICKS */


    //Ports
    public static final int PORT_JOYSTICK_LEFT = 0;
    public static final int PORT_JOYSTICK_RIGHT = 1;
    public static final int PORT_JOYSTICK_SHOOTER = 2;

    //Joysticks
    public static Joystick JOYSTICK_LEFT = new Joystick(PORT_JOYSTICK_LEFT);
    public static Joystick JOYSTICK_RIGHT = new Joystick(PORT_JOYSTICK_RIGHT);
    public static Joystick JOYSTICK_SHOOTER = new Joystick(PORT_JOYSTICK_SHOOTER);

    /* PNEUMATICS */
    public static DoubleSolenoid SHOOTER_ARM = new DoubleSolenoid(0,1); //Port 0 & 1

    /* POTENTIOMETER */
    public static final int POTENTIOMETER_OFFEST = 0;
    //public static Potentiometer POTENTIOMETER = new AnalogPotentiometer(new AnalogInput(0), 360, POTENTIOMETER_OFFEST);  //AnalogInput, Factor to scale 0-1, Offset
    public static AnalogInput POTENTIOMETER = new AnalogInput(0);

}
