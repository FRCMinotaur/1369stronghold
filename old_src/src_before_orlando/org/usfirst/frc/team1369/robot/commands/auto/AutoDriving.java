package org.usfirst.frc.team1369.robot.commands.auto;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team1369.robot.Robot;
import org.usfirst.frc.team1369.robot.subsystems.DriveTrain;

/**
 * Created by Ezekiel on 15-Feb-16.
 */
public class AutoDriving extends Command {

    private boolean isDone;
    private DriveTrain driveTrain;
    private Lift lift;
    private Shooter shoot;
    private ShooterArm sa;
    private ADXRS450_Gyro gyro;
    private final double Kp = 0.03;
    private final double Kpi = 0.01960784313;
    

    public AutoDriving() {
        driveTrain = Robot.driveTrain;
        lift  = Robot.lift;
        sa = Robot.shoterarm;
        shoot = Robot.shoot;
        this.requires(driveTrain);
        this.requires(lift);
        this.requires(shoot);
        this.requires(sa);
        gyro = Robot.gyro;
    }

    @Override
    protected void initialize() {
        isDone = false;
         new Thread(new Runnable() {
            @Override
            public void run() {
             //    double angle = gyro.getAngle(); // get current heading
               // driveTrain.driveCustom((-angle * (Kp * 0.003)) + (.15), (-angle * (Kp * 0.003)) + (-.15)); // drive towards heading 0
                Timer.delay(0.004);
            //    sleep(2000);
             //   driveTrain.stop();
            //    sleep(250);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        lift.liftDownwards(.5)
                        sleep(500);
                        lift.forceStop();
                    }
                  }).start();
                driveTrain.driveDistance(true, 0.5, 226.898*kpi);
                boolean hasTurned = driveTrain.turningByRate(-60
                        , 0.5);
                while (hasTurned == false) {
                    if (hasTurned) break;
                }
                driveTrain.driveDistance(true, 0.5, 97.802*kpi);
                shoot.setSpeed(5);
                shoot.outtake();
                sleep(10);
                sa.activate();
                shoot.shootCustom(0);
                boolean hasTurned = driveTrain.turningByRate(180
                        , 0.5);
                while (hasTurned == false) {
                    if (hasTurned) break;
                }
                  driveTrain.driveDistance(true, 0.5, 97.802*kpi);
                    boolean hasTurned = driveTrain.turningByRate(-60
                        , 0.5);
                while (hasTurned == false) {
                    if (hasTurned) break;
                }
                driveTrain.driveDistance(true, 0.5, 226.898*kpi);
                
                
            }
        }).start();
    }

    @Override
    protected void execute() {
       
           
    }

    public void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected boolean isFinished() {
        return isDone;
    }

    @Override
    protected void end() {
        Robot.driveTrain.stop();
    }

    @Override
    protected void interrupted() {
        Robot.driveTrain.stop();
    }
}
