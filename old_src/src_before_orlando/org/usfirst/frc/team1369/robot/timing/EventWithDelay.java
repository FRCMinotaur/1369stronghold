package org.usfirst.frc.team1369.robot.timing;

/**
 * @author Cheenar
 * @description EventWithDelay will run a runnable after a delay
 * @notes Useful in Autonomous if you want to wait a duration of time before running something
 */

public class EventWithDelay
{

    private Runnable runnable;
    private int delay;

    public EventWithDelay(Runnable runnable, int delay)
    {
        this.runnable = runnable;
        this.delay = delay;
    }

    public Runnable getRunnable()
    {
        return this.runnable;
    }

    public void start()
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    Thread.sleep(delay);
                    new Thread(runnable).start();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    System.out.println("EventWithDelay failed");
                }
            }
        }).start();
    }

}
